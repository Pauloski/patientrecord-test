import React from 'react';
import style from './selectbox.module.scss'

const SelectBox = ({ onChange, value, optionData }) => {
  return (
<div className={ `${style[`select`]}` }>
 <select  onChange={onChange} value={value}>
    <option value="0"  description='Select User' key ='0 '>
    Select User
    </option>
     { 
       optionData.map((item, index) =>
        <option value={item.userId}  description={item.userName} key ={index} >
         {item.userName}
        </option>

       )
     }  
  </select>
  </div>
  )
}

export default SelectBox;
