import React from 'react';
import PropTypes from 'prop-types';
import style from './button.module.scss'
export const typeButton = ['primary-xxs', 'primary-xs','primary-sm', 'primary-md', 'primary-lg','primary-xl','primary-xxl'];
export const disabled = [false, true];

const Button = ({testId, textButton, typeButton, actionButton, disabled}) => {
  return (
    <button data-testid={testId} 
      className={ `${style[`button--${typeButton}`]} ${style[`button`]}` } 
      onClick={actionButton} disabled={disabled}>
     {textButton}
  </button>
  )
}
Button.propTypes = {
  typeButton: PropTypes.oneOf(['primary-xxs', 'primary-xs','primary-sm', 'primary-md', 'primary-lg','primary-xl','primary-xxl']),
  textButton: PropTypes.string,
  testId: PropTypes.string,
  actionButton: PropTypes.any,
  disabled: PropTypes.bool,
}

Button.defaultProps = {
  typeButton: 'primary-md',
  textButton: 'Show',
  testId: 'show-patient',
  disabled: false,

}

export default Button;
