import React from 'react';
import Title from '../../atoms/Title';
import style from './header.module.scss';

const Header = () => {
  return (
  <div className={ `${style[`header`]}` }>
    <Title textComponent="Patient Record" seoType="h1" fontSize="medium" weight="bold" />
  </div>
  )
}

export default Header;
