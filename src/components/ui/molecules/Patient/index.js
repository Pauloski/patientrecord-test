import React from 'react';
import Title from '../../atoms/Title';
import style from './patient.module.scss';

const Patient = ({userName, dob, height, key}) => {
  return (
  <div className={ `${style[`patient-wrapper`]}` }  key ={key}> 
    <Title textComponent={userName} fontSize="medium" align="center"  weight="bold"/>
    <Title textComponent={dob} fontSize="normal" align="center" />
    <Title textComponent={height}  fontSize="small" align="center"/>
  </div>
  )
}

export default  Patient;
