import React from 'react';
import Button from '../../atoms/Button';
import SelectBox from '../../atoms/SelectBox';
import style from './search.module.scss';

const SearchPatient = ({onChange, value, optionData, actionButton, disabled }) => {
  return (
  <div className={ `${style[`search-wrapper`]}` }>
     <div className={ `${style[`search-wrapper__select`]}` }>
      <SelectBox onChange={onChange} value={value} optionData={optionData} />
     </div>
     <div className={ `${style[`search-wrapper__button`]}` }>
     <Button typeButton="primary-xs" actionButton={actionButton} disabled = {disabled} />
       
     </div>
  
  </div>
  )
}

export default  SearchPatient;
