import React, {Component} from 'react';
import SearchPatient from '../../ui/molecules/SearchPatient';
import Patient from '../../ui/molecules/Patient';


class PatientRecord extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: [], 
      activeIndex: false,
      value:0,
      showPatient: [],
      activePatient: false,
      disabled: true,
      viewport: '',
     
    };
  }


  componentDidMount() {

    var patient = 'https://jsonmock.hackerrank.com/api/medical_records';
    fetch(patient)
    .then(response => response.json())
    .then((items) => {
    
    this.setState({ activeIndex: true })
    function getUnique(arr, comp) {
    const unique =  arr.map(e => e[comp])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter((e) => arr[e]).map(e => arr[e]);
    return unique;
}
    this.setState({ data: getUnique(items.data,'userId')});
    });

    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
  }
 
  handleChange = e => {
    this.setState({ value: e.target.value });
    if(e.target.value > 0){ 
      this.setState({ disabled: false })
    }else {
      this.setState({ disabled: true})
    }
    
  
  };

  getPatient = () => {
    this.setState({ activeIndex: false });
    var url = 'https://jsonmock.hackerrank.com/api/medical_records?userId='+this.state.value;
    //console.log(url)
    fetch(url)
    .then(response => response.json())
    .then((item) => {
    this.setState({ showPatient: item.data});
    this.setState({ activePatient: true });
    this.setState({ activeIndex: true });
   
    })
  
  };
  

getFormattedDate = (timestamp) => {
    const date = new Date(timestamp);
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    const yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
};

resize() {
  if(window.innerWidth <= 600) {
    this.setState({viewport: 'isMobileXS'});
  }else {
    this.setState({viewport: 'isMobileHD'});
  }
  
}

  render(){
    const { activeIndex, data, activePatient, showPatient, disabled, viewport} = this.state;
    if(!activeIndex) {
      return (
        <div className="loader" />
        )
    } else {
      return (
        <>
       
        <SearchPatient  disabled ={disabled} onChange={this.handleChange} value={this.state.value} optionData={data} actionButton={this.getPatient}  />
        { activePatient ?
        <div className="list-wrapper"> 
          <Patient userName={showPatient[0].userName} dob={'Dob: '+showPatient[0].userDob} height={'Height:'+ showPatient[0].meta.height} />
          <div class="container">
          { viewport==='isMobileXS' ? 
          <div className="is-mobile-xs">
        { 
          showPatient.map((item, index) =>
       <div className="user-card" key={index}>
        <div className="user-card__row" data-title="SL"><strong>SL: </strong>{item.id}</div>
        <div className="user-card__row" data-title="Date"><strong>Date: </strong>{ this.getFormattedDate(item.timestamp)}</div>
        <div className="user-card__row" data-title="Diagnosis" ><strong>Diagnosis: </strong>{item.diagnosis.name + ' ('+item.diagnosis.severity+')'}</div>
        <div className="user-card__row" data-title="Weight" ><strong>Weight: </strong> {item.meta.weight}</div>
        <div className="user-card__row" data-title="Doctor" ><strong>Doctor: </strong> {item.doctor.name}</div>
      </div>
        )
          } 
          </div>  :
           <div className="is-mobile-hd">
            
            <table class="responsive-table">
              <thead>
                <tr>
                  <th scope="col">SL</th>
                  <th scope="col">Date</th>
                  <th scope="col">Diagnosis</th>
                  <th scope="col">Weight</th>
                  <th scope="col">Doctor</th>
                </tr>
              </thead>
              <tbody>
         
          { 
          showPatient.map((item, index) =>
       <tr key={index}>
        <td data-title="SL">{item.id}</td>
        <td data-title="Date">{ this.getFormattedDate(item.timestamp)}</td>
        <td data-title="Diagnosis" >{item.diagnosis.name + ' ('+item.diagnosis.severity+')'}</td>
        <td data-title="Weight" >{item.meta.weight}</td>
        <td data-title="Doctor" >{item.doctor.name}</td>
      </tr>
        )
          } 
        </tbody>
      </table>



          </div>
           }
           
      </div>   
        </div> 
        : 
        <div></div> }
      </>
        ) 
    } 
  }
}

export default PatientRecord;