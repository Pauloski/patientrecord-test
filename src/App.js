import React from 'react';
import PatientRecord from './components/page/PatientRecord';
import Header from './components/ui/molecules/Header';
import './styles/App.scss';

function App() {
  return (
   <> 
   <Header />
    <div className="App">
      <PatientRecord />
    </div>
    </>
  );
}

export default App;
