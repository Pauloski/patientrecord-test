# Test Front-End | Yaap

Test Front-End
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) Muestre un listado de
colaboradores (al menos 10) que debes extraer desde la API gratuita randomuser.me. Al seleccionar alguna persona se debe mostrar el detalle de la misma en una ficha descriptiva.
## Summary

  - [Getting Started](#getting-started)


## Getting Started

1.- Download master branch of the repositorie.

2.- Run npm i (npm install). for install dependencies

3.- Run  yarn start. Runs the app in development mode. Open http://localhost:3000 to view it in the browser.

4.- yarn run storybook. Runs the Storybook 5.3.18 started Local: http://localhost:9000/